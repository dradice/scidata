import fnmatch
import math
import numpy
import numpy.ma
import os
import re
import sys

class Struct(object):
    pass

class FileTypeError(Exception):
    """
    Data file type not supported
    """
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def advanced_time(t, M_Inf=2.7, R_GW=400.0):
    """
    Gets the advanced time
    """
    R = R_GW * (1 + M_Inf/(2*R_GW))**2
    rstar = R + 2*M_Inf*math.log(R/(2*M_Inf) - 1)
    return t + rstar

def align_phase(t, t_min, t_max, phi_a_tau, phi_b):
    """
    Align two waveforms in phase by minimizing the chi^2

        \chi^2 = \int_{t_min}^{t_max} [\phi_a(t + \tau) - phi_b(t) - \Delta\phi]^2 dt

    as a function of \Delta\phi.

    * t              : time, must be equally spaced
    * t_min, t_max   : time range over which to align
    * phi_a_tau      : time-shifted first phase evolution
    * phi_b          : second phase evolution

    This function returns \Delta\phi.
    """
    dt = t[1] - t[0]
    weight = numpy.double((t >= t_min) & (t < t_max))
    return numpy.sum(weight * (phi_a_tau - phi_b) * dt) / \
           numpy.sum(weight * dt)

def align(t, t_min, t_max, tau_max, t_a, phi_a, t_b, phi_b):
    """
    Align two waveforms in phase by minimizing the chi^2

        chi^2 = \sum_{t_i=t_min}^{t_i < t_max} [phi_a(t_i + tau) - phi_b(t_i) - dphi]^2 dt

    as a function of dphi and tau.

    * t              : time
    * t_min, t_max   : time range over which to align
    * tau_max        : maximum time shift
    * t_a, phi_a     : first phase evolution
    * t_b, phi_b     : second phase evolution

    The two waveforms are re-sampled using the given time t

    This function returns a tuple (tau_opt, dphi_opt, chi2_opt)
    """
    dt = t[1] - t[0]
    N = int(tau_max/dt)
    weight = numpy.double((t >= t_min) & (t < t_max))

    res_phi_b = numpy.interp(t, t_b, phi_b)

    tau = []
    dphi = []
    chi2 = []
    for i in range(-N, N):
        tau.append(i*dt)
        res_phi_a_tau = numpy.interp(t, t_a + tau[-1], phi_a)
        dphi.append(align_phase(t, t_min, t_max, res_phi_a_tau, res_phi_b))
        chi2.append(numpy.sum(weight*
            (res_phi_a_tau - res_phi_b - dphi[-1])**2)*dt)

    chi2 = numpy.array(chi2)
    imin = numpy.argmin(chi2)

    return (tau[imin], dphi[imin], chi2[imin])

def basename(filename):
    """
    Get the base name of the given filename
    """
    return re.match(r"(.+)\.(\w+)", filename).group(1)

def bisection(func, domain, eps=sys.float_info.epsilon, nmax=52):
    """
    A simple one-dimensional root finder based on the bisection method

    * func   : must be a callable function returning a float
    * domain : must be a tuple (xmin, xmax) within which the zero of func
               is located
    * eps    : stop when the residual falls below this value
    * nmax   : maximum number of iterations
    """
    x1, x2 = domain

    fx1 = func(x1)
    fx2 = func(x2)

    if fx1*fx2 > 0:
        raise ValueError("func does not change sign between the extrema " +
                "of the domain")
    elif fx1 == 0:
        return x1
    elif fx2 == 0:
        return x2

    it  = 0
    xm  = 0.5*(x1 + x2)
    fxm = func(xm)

    while(it < nmax and abs(fxm) > eps):
        it += 1

        if fxm*fx1 > 0:
            x1 = xm
        elif fxm == 0:
            return xm
        else:
            x2 = xm

        xm  = 0.5*(x1 + x2)
        fxm = func(xm)

    return xm

def chirp_mass(M1, M2):
    """
    Computes the chirp mass of a binary given M1 and M2
    """
    M = M1 + M2
    mu = M1*M2/M
    return mu**(3./5.)*M**(2./5.)

def collate(fnames, tidx=0, comments=["#", "%"], include_comments=False,
        epsilon=1e-15):
    """\
    Merge a list of files from multiple segments

    * fnames   : list of file names, must be sorted from earlier to later
                 output
    * tidx     : time column number (starting from zero)
    * comments : lines beginning with a comment symbol are considered to be
                 comments
    * include_comments :
                 include comments in the output
    * epsilon  : two output times are the same if they differ by less than
                 epsilon

    Returns a string with the merged files
    """
    sout = []
    told  = None

    for fname in reversed(fnames): # Read backwards so new data 'overwrites' old.
        for dline in reversed(open(fname).readlines()):
            skip = False
            for c in comments:
                if dline[:len(c)] == c:
                    if include_comments:
                        sout += dline
                    skip = True
                    break
            if skip:
                continue

            try:
                tnew = float(dline.split()[tidx])
            except IndexError:
                continue
            if told is None or tnew < told*(1 - epsilon):
                sout.append(dline)
                told = tnew
    return ''.join(reversed(sout)) # Reverse again to time-ordered

def compute_streamlines(x, y, u, v, n=10, seed_type='regular', lfac=1):
    """
    Computes the streamlines of [u, v] on the regular grid given by x, y

    * x         : column coordinates
    * y         : row coordinates
    * u         : x-component of the vector field
    * v         : y-component of the vector field
    * n         : number of seeds
    * seed_type : seeds type
    * lfac      : how long to keep on integrating

    Supported seed_type are
    * random : randomly placed
    * regular : on a regular grid
    * x : start from the x-axis
    * y : start from the y-axis

    Returns a list of lines [(x, y)]
    """
    import scipy.interpolate
    import scipy.integrate

    uint = scipy.interpolate.RectBivariateSpline(x[:, 0], y[0,:], u, s=0)
    vint = scipy.interpolate.RectBivariateSpline(x[:, 0], y[0,:], v, s=0)

    # This is supposed to be more efficient but it doesn't work
    # (and it is also much slower!)
    #uint = scipy.interpolate.interp2d(x.transpose(), y.transpose(),\
    #        u.transpose(), copy=False, fill_value=0)
    #vint = scipy.interpolate.interp2d(x.transpose(), y.transpose(),\
    #        v.transpose(), copy=False, fill_value=0)

    f = lambda y, t: [uint(y[0], y[1])[0, 0], vint(y[0], y[1])[0, 0]]
    g = lambda y, t: [-uint(y[0], y[1])[0, 0], -vint(y[0], y[1])[0, 0]]

    xmin = x.min()
    xmax = x.max()
    ymin = y.min()
    ymax = y.max()

    isin = lambda y : y[0] >= xmin and y[0] <= xmax and\
            y[1] >= ymin and y[1] <= ymax

    if seed_type == 'random':
        seeds = numpy.array([xmin, ymin]) + numpy.random.random((n*n, 2)) *\
            numpy.array([xmax - xmin, ymax - ymin])
    elif seed_type == 'x':
        xs = numpy.arange(xmin, xmax, (xmax - xmin) / (2*n))
        ys = ymin * numpy.ones(xs.shape)
        seeds = numpy.column_stack((xs.flatten(), ys.flatten()))
    elif seed_type == 'y':
        ys = numpy.arange(ymin, ymax, (ymax - ymin) / (2*n))
        xs = xmin * numpy.ones(ys.shape)
        seeds = numpy.column_stack((xs.flatten(), ys.flatten()))
    else:
        xs = numpy.arange(xmin, xmax, (xmax - xmin) / n)
        ys = numpy.arange(ymin, ymax, (ymax - ymin) / n)
        xs, ys = numpy.meshgrid(xs, ys)
        seeds = numpy.column_stack((xs.flatten(), ys.flatten()))

    out = []

    Tfin = lfac
    dt = 0.01*Tfin
    t = numpy.arange(0, Tfin, dt)

    for xy in seeds:
        line = scipy.integrate.odeint(f, xy, t)
        data = []
        for i in range(len(line.T[0])):
            if isin((line.T[0][i], line.T[1][i])):
                data.append((line.T[0][i], line.T[1][i]))
            else:
                break
        out.append(([x[0] for x in data], [x[1] for x in data]))

        line = scipy.integrate.odeint(g, xy, t)
        data = []
        for i in range(len(line.T[0])):
            if isin((line.T[0][i], line.T[1][i])):
                data.append((line.T[0][i], line.T[1][i]))
            else:
                break
        out.append(([x[0] for x in data], [x[1] for x in data]))

    return out

def dict2array(d, dtype=numpy.float64):
    """
    Converts a dictionary "d" with tuple of integer keys to a 1D
    array "a" such that:
        a[i,j,...] = d[i,j,...]     if i,j,... in d.keys()
        a[i,j,...] = masked         otherwise
    """
    assert (isinstance(d, dict))
    assert (isinstance(list(d.keys())[0], tuple))
    shape = [0 for i in list(d.keys())[0]]
    for k in list(d.keys()):
        assert (len(k) == len(shape))
        for i in range(len(k)):
            assert (k[i] >= 0)
            shape[i] = max(shape[i], k[i])
    for i in range(len(shape)):
        shape[i] += 1

    a = numpy.empty(tuple(shape), dtype=dtype)
    mask = numpy.zeros(tuple(shape), dtype=numpy.bool)
    mask[:] = True

    for k in list(d.keys()):
        a[k] = d[k]
        mask[k] = False

    return numpy.ma.masked_array(a, mask=mask)

def diff(ff):
    """
    Computes the undivided difference of ff using a 2nd order formula
    """
    if len(ff.shape) != 1:
        raise ValueError("diff only operates on 1D arrays")
    out = numpy.empty_like(ff)
    out[1:-1] = ff[2:] - ff[:-2]
    out[0] = -1.5*ff[0] + 2*ff[1] - 0.5*ff[2]
    out[-1] = 1.5*ff[-1] - 2*ff[-2] + 0.5*ff[-3]
    return out

def diff1(xp, yp):
    """
    Computes the first derivative of y(x) using centered 2nd order
    accurate finite-differencing

    This function returns an array of yp.shape[0]-2 elements

    NOTE: the data needs not to be equally spaced
    """
    dyp = [(yp[i+1] - yp[i-1])/(xp[i+1] - xp[i-1]) \
            for i in range(1, xp.shape[0]-1)]
    return numpy.array(dyp)

def diff2(xp, yp):
    """
    Computes the second derivative of y(x) using centered 2nd order
    accurate finite-differencing

    This function returns an array of yp.shape[0]-2 elements

    NOTE: the data needs not to be equally spaced
    """
    ddyp = [4*(yp[i+1] - 2*yp[i] + yp[i-1])/((xp[i+1] - xp[i-1])**2) \
            for i in range(1, xp.shape[0]-1)]

    return numpy.array(ddyp)

def extension(filename):
    """
    Get the extension of the given filename
    """
    return re.match(r"(.+)\.(\w+)", filename).group(2)

# From Reisswig and Pollney, Class. Quantum Grav. 28 (2011) 195015
def fixed_freq_int_1(signal, cutoff, dt=1):
    """
    Fixed frequency time integration

    * signal : a numpy array with the target signal
    * cutoff : the cutoff frequency
    * dt     : the sampling of the signal
    """
    from scipy.fftpack import fft, ifft, fftfreq

    f = fftfreq(signal.shape[0], dt)

    idx_p = numpy.logical_and(f >= 0, f < cutoff)
    idx_m = numpy.logical_and(f <  0, f > -cutoff)

    f[idx_p] = cutoff
    f[idx_m] = -cutoff

    return ifft(-1j*fft(signal)/(2*math.pi*f))

# From Reisswig and Pollney, Class. Quantum Grav. 28 (2011) 195015
def fixed_freq_int_2(signal, cutoff, dt=1):
    """
    Fixed frequency double time integration

    * signal : a numpy array with the target signal
    * cutoff : the cutoff frequency
    * dt     : the sampling of the signal
    """
    from scipy.fftpack import fft, ifft, fftfreq

    f = fftfreq(signal.shape[0], dt)

    idx_p = numpy.logical_and(f >= 0, f < cutoff)
    idx_m = numpy.logical_and(f <  0, f > -cutoff)

    f[idx_p] = cutoff
    f[idx_m] = -cutoff

    return ifft(-fft(signal)/(2*math.pi*f)**2)

def indent(string):
    """
    Indents one string
    """
    s = ""
    for q in string.split("\n"):
        s += "\t" + q + "\n"
    return s[0:-1]

def integrate(ff):
    """
    Computes the anti-derivative of a discrete function using a
    2nd order accurate formula
    """
    out = numpy.empty_like(ff)
    out[0] = 0.0
    out[1:] = numpy.cumsum(0.5*(ff[:-1] + ff[1:]))
    return out

## {{{ http://code.activestate.com/recipes/499305/ (r3)
def ilocate(pattern, root=os.curdir, followlinks=False):
    '''Locate all files matching supplied filename pattern in and below
    supplied root directory.'''
    for path, dirs, files in os.walk(os.path.abspath(root),
            followlinks=followlinks):
        for filename in fnmatch.filter(files, pattern):
            yield os.path.join(path, filename)
## end of http://code.activestate.com/recipes/499305/ }}}

def locate(pattern, root=os.curdir, followlinks=False):
    '''Locate all files matching supplied filename pattern in and below
    supplied root directory.'''
    myp = pattern.replace("[", "?")
    myp = myp.replace("]", "?")
    return sorted([f for f in ilocate(myp, root, followlinks)])

def masses_from_Mchirp_q(Mchirp, q):
    """
    Computes M1 and M2 given the chirp mass of a binary and the mass ratio q
    """
    assert(np.all(q <= 1))
    M1 = ((1. + q)**(1./5))/(q**(3./5.))*Mchirp
    M2 = q*M1
    return M1, M2

def makelist(stuff):
    """
    Smarter converter to list
    """
    try:
        return list(stuff)
    except:
        return [stuff]

def maskdata(data, stencil):
    """
    Creates a masked array representing a subsample of the data on the given
    stencil

    * data    : must be a multidimensional numpy.array
    * stencil : must be an array containing the stencil width in every
                direction
    """
    sl = []
    for d in range(len(data.shape)):
        sl.append(slice(0, data.shape[d], stencil[d]))
    sl = tuple(sl)

    mask     = numpy.isfinite(data)
    mask[sl] = numpy.logical_not(mask[sl])
    return numpy.ma.masked_array(data, mask=mask)

def print_table(table):
    """
    Prints a table using printtable.Printtable

    * table: must be a list of columns
    """
    widths = []
    for i in range(len(table[0])):
        w = 1
        for x in table:
            y = x[i].split('\n')
            w = max([w] + [len(q) for q in y])
        widths.append(w)

    import texttable

    ttable = texttable.Texttable()
    ttable.add_rows(table)
    ttable.set_cols_width(widths)
    return ttable.draw()

def retarded_time(t, M_Inf=2.7, R_GW=400.0):
    """
    Gets the retarded time
    """
    R = R_GW * (1 + M_Inf/(2*R_GW))**2
    rstar = R + 2*M_Inf*math.log(R/(2*M_Inf) - 1)
    return t - rstar

def rotate(X, Y, data, theta, xc = 0, yc = 0):
    """
    Rotates the grid data by an angle omega in a counter-clockwise direction
    about (xc,yc)

    * X     : 2D numpy array of x-coordinates
    * Y     : 2D numpy array of y-coordinates
    * data  : Data sampled on the (X,Y) grid
    * omega : Angle (in radiants)
    * xc    : x-coordinate of the center of rotation
    * yc    : y-coordinate of the center of rotation
    """
    func = scipy.interpolate.RectBivariateSpline(X[:, 0], Y[0,:], data)

    ct = math.cos(theta)
    st = math.sin(theta)

    out = []
    for y in Y[0,:]:
        for x in X[:, 0]:
            yp = (x-xc)*ct - (y-yc)*st + xc
            xp = (x-xc)*st + (y-yc)*ct + yc
            out.append(float(func(xp, yp)))

    out = numpy.array(out)
    return out.reshape(data.shape)

def streamlines(x, y, u, v, n=10, seed_type='regular', style='k-', lfac=1,\
        **kwargs):
    """
    Creates a streamline plot

    * x         : column coordinates
    * y         : row coordinates
    * u         : x-component of the vector field
    * v         : y-component of the vector field
    * n         : number of seeds
    * seed_type : seeds type
    * style     : plot line style
    * lfac      : how long to keep on integrating
    """
    import matplotlib

    lines = compute_streamlines(x, y, u, v, n, seed_type, lfac)
    pltlist = []
    for l in lines:
        pltlist.append(matplotlib.pyplot.plot(l[0], l[1], style, **kwargs))
    return pltlist

def subsample(data, stencil):
    """
    Subsample the data on a given stencil

    * data    : must be a multidimensional numpy.array
    * stencil : must be an array containing the stencil width in every
                direction
    """
    sl = []
    for d in range(len(data.shape)):
        sl.append(slice(0, data.shape[d], stencil[d]))
    sl = tuple(sl)

    return data[sl]

## {{ http://stackoverflow.com/questions/8560440/removing-duplicate-columns-and-rows-from-a-numpy-2d-array
def sorted_array(array):
    """
    Returns a copy of the original vector which is

    * Sorted according to the lexicographical ordering
    * Purged of all duplicated elements
    """
    idx  = numpy.lexsort(array.T[::-1])
    da   = numpy.diff(array[idx], axis=0)
    cond = numpy.any(da, axis=1)
    cond = numpy.append(cond, True)
    return array[idx][cond]
## end of http://stackoverflow.com/questions/8560440/removing-duplicate-columns-and-rows-from-a-numpy-2d-array}}

def unmask(array):
    """
    Unmasks an numpy.array
    """
    if isinstance(array, numpy.ma.core.MaskedArray):
        return array[array.mask == False].data
    else:
        return array

def voronoi_volume(points, maxvol=0):
    """
    Computes the volume associated with each point according to a Voronoi
    tasselation

    * points : ndarray with shape (npoints, ndim)
    * maxvol : volume assigned to points at the outer edge of the tasselation
    """
    from scipy.spatial import ConvexHull, Voronoi

    npoints = points.shape[0]
    volumes = numpy.ones(npoints, dtype=points.dtype)*maxvol
    vor = Voronoi(points)
    for ip in range(npoints):
        vindices = vor.regions[vor.point_region[ip]]
        if -1 not in vindices:
            hull = ConvexHull(vor.vertices[vindices])
            volumes[ip] = hull.volume
    if maxvol > 0:
        volumes = np.minimum(volumes, maxvol)
    return volumes

def cut_off_freq(mmode,cutoff_base):
    """
    cut off freq. for fixed freq. integration
    ref: https://arxiv.org/pdf/1006.1632: above sec 4.3

    * mmode: m mode of the waveform
    * cutoff_base: base cut off freq.
    """

    mfact = abs(mmode)
    mfact = max(mfact,1)
    cutoff = cutoff_base * mfact

    return cutoff

def perturb_extrap_ffi(t, R_psi4, R, l, m, M, cut_off):
    """
    Perform perturbative method to extrapolation R*psi4 at inf radius
    it uses fix freq. integration so less modulation is introduced

    Extrapolates Psi4 to infinite radius following

            ref1: arxiv:1008.4360v2, Eq. 3
            ref2: arXiv:1708.08926, Eq. 2.10

    Input
            * t, R_psi4  : time and complex (l,m)-mode of R psi4
            * R          : extraction radius
            * l, m       : multipole indices
            * M          : M = m1+m2 where m1 and m2 are BH's masses
            * cut_off    : base cut off freq. for FFI
    Output
            * R*Psi4_inf  : extrapolated R psi4 to R -> oo
    """

    rA = R * (1.0 + M / (2.0 * R))**2 ## use ref2
    C = 1.0 - 2 * M / rA ## use ref2, it seems there is a typo in ref1.
    dt = t[2] - t[1]
    R_dh = fixed_freq_int_1(R_psi4, cut_off_freq(m,cut_off), dt)

    return C * (R_psi4 - (l - 1.0) * (l + 2.0) / (2.0 * rA) * R_dh)
