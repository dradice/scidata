import numpy

def exponential(x, delta=0):
    """
    Double side, exponential windowing function on [0,1]
    """
    return numpy.piecewise(x,
            [(x == 0),
             (x > 0) & (x < delta),
             (x >= delta) & (x <= 1.0 - delta),
             (x > 1.0 - delta) & (x < 1),
             (x == 1)],
            [lambda x: 0.0,
             lambda x: numpy.exp(-1.0/(1.0 - ((x-delta)/delta)**2) + 1.0),
             lambda x: 1.0,
             lambda x: numpy.exp(-1.0/(1.0 - ((x-(1.0-delta))/delta)**2) + 1.0),
             lambda x: 0.0]
            )
