"""
WARNING: this module only works for uniform meshes with cubic blocks
"""

import h5py
import numpy as np

def read_file(fname):
    """
    Read data from a FLASH unigrid HDF5 file
    """
    dfile = h5py.File(fname, 'r')
    grid  = UniformGrid(dfile)
    names = read_names(dfile)
    data  = {}
    for name in names:
        data[name] = read_uniform_data(dfile, grid, name)
    return data

def read_names(dfile):
    """
    Returns the list of available field names from a FLASH HDF5 file

    * dfile : h5py.File object to read
    """
    assert isinstance(dfile, h5py.File)
    unkwn = np.array(dfile['unknown names'])
    names = []
    for idx in range(unkwn.shape[0]):
        names.append(unkwn[idx, 0])
    return names

def read_ugrid(dfile):
    """
    Read a uniform grid from a FLASH HDF5 file

    * dfile : h5py.File object to read
    """
    assert isinstance(dfile, h5py.File)
    return UniformGrid(dfile)

def read_uniform_data(dfile, ugrid, field, dtype=None):
    """
    Read data from a FLASH unigrid HDF5 file

    * dfile : h5py.File object to read
    * ugrid : a flash.UniformGrid object
    * field : name of the field to read
    * dtype : convert data to a given datatype
              (if None no conversion is performed)
    """
    if dtype is None:
        dtype = ugrid.dtype
    out    = np.empty(ugrid.npoints, dtype=dtype)
    out[:] = np.NAN
    for block in ugrid.blocks:
        block.restrict(out)[:] = read_block(dfile, block.idx, field)
    return out

def read_block(dfile, bid, name):
    """
    Read a block of data from a FLASH HDF5 file

    * dfile : h5py.File object to read
    * bid   : index of the block to read
    * field : name of the field to read
    """
    return np.array(dfile[name][bid]).transpose()

def read_list(dfile, name):
    """
    Read a list from a FLASH HDF5
    """
    assert isinstance(dfile, h5py.File)
    return dict([(k.strip(), v) for k, v in list(dfile[name])])

def read_runtime_parameters(dfile):
    """
    Read the list of runtime parameters from a FLASH HDF5
    """
    return read_list(dfile, r'real runtime parameters')

def read_scalars(dfile):
    """
    Read a list of real scalars from a FLASH HDF5
    """
    return read_list(dfile, r'real scalars')

class Block:
    """
    Single logical grid block
    """
    def __init__(self, idx, ndim, ibbox):
        """
        Initialize a grid box

        * idx         : block index
        * ndim        : number of spatial dimensions
        * ibbox[0][.] : index of the first grid point in the global enum
        * ibbox[1][.] : index of the last grid point in the global enum
        """
        self.idx    = idx
        self.ndim   = ndim
        self.mslice = []
        for d in range(self.ndim):
            self.mslice.append(slice(ibbox[0][d], ibbox[1][d]))
        self.mslice = tuple(self.mslice)
    def restrict(self, array):
        """
        Restrict an array to the current block
        """
        return array[self.mslice]

class UniformGrid:
    """
    Uniform grid with fixed block size

    * blocks  : list of flash.Blocks
    * delta   : grid spacing
    * dtype   = data type
    * ndim    : number of spatial dimensions
    * npoints : number of grid points in each direction
    * origin  : grid origin
    """
    def __init__(self, dfile):
        """
        Read an uniform grid from an HDF5 file

        * dfile : must be of type h5py.File
        """
        assert isinstance(dfile, h5py.File)

        block_size = np.array(dfile['block size'])[0]
        var_name   = np.array(dfile['unknown names'])[0, 0]
        nblocks    = dfile[var_name].shape[0]
        block_np   = dfile[var_name].shape[1:]
        self.dtype = dfile[var_name].dtype

        # Compute the grid spacing from the first block
        self.ndim  = block_size.shape[0]
        self.delta = np.zeros(self.ndim, dtype=self.dtype)
        for d in range(self.ndim):
            self.delta[d] = block_size[d]/float(block_np[d])

        # Get the origin
        params = read_runtime_parameters(dfile)
        self.origin = np.zeros(self.ndim, dtype=self.dtype)
        self.origin[0] = params['xmin']
        if self.ndim > 1:
            self.origin[1] = params['ymin']
        if self.ndim > 2:
            self.origin[2] = params['zmin']

        # Compute the number of grid points in each direction
        self.npoints = np.zeros(self.ndim, dtype=int)
        self.npoints[0] = self.index(0, params['xmax'])
        if self.ndim > 1:
            self.npoints[1] = self.index(1, params['ymax'])
        if self.ndim > 2:
            self.npoints[2] = self.index(2, params['zmax'])
        self.npoints = tuple(self.npoints)

        # Create logical grid blocks
        bbox = np.array(dfile['bounding box'])
        self.blocks = []
        for bid in range(bbox.shape[0]):
            ibbox    = [[], []]
            ibbox[0] = np.zeros(self.ndim, dtype=int)
            ibbox[1] = np.zeros(self.ndim, dtype=int)
            for d in range(self.ndim):
                ibbox[0][d] = self.index(d, bbox[bid][d, 0])
                ibbox[1][d] = self.index(d, bbox[bid][d, 1])
            self.blocks.append(Block(bid, self.ndim, ibbox))

    def index(self, dr, val):
        """
        Compute the index associated to a given offset in a given direction

        * dr  : direction (0, 1 or 2)
        * val : value of the dr-th coordinate
        """
        return int(round((val - self.origin[dr])/self.delta[dr]))

    def mesh(self):
        """
        Returns a full mesh.

        This returns a tuple of np.arrays containing the value of the
        coordinates in the various points of the grid
        """
        sl = []
        for d in range(self.ndim):
            sl.append(slice(0, self.npoints[d]))

        # This is a trick to convert mgrid to float
        L = np.mgrid[sl]*1.0
        for d in range(self.ndim):
            L[d] = self.origin[d] + self.delta[d]*L[d]

        return tuple(L)
