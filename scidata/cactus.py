import csv
import os
import re

class parfile:
    """
    Stores the contents of a Cactus parfile

    Members:
    * name    : filename
    * path    : full path
    * options : a list of tuples (thorn, option, [values]) containing the
                details of the parfile
    * thorns  : list of activated thorns
    """

    def __init__(self, filename):
        """
        Initialize the parfile
        """
        self.name = os.path.basename(filename)
        self.path = os.path.abspath(os.path.dirname(filename))
        self.options = []
        self.thorns = []

        rawdata = open(os.path.join(self.path, self.name), 'r').readlines()

        # remove newlines and fix capitalization
        rawdata = [x[:-1].lower() for x in rawdata]

        # remove comments
        filtered = []
        for l in rawdata:
            filtered.append(re.match(r"([^#]*)#?([^#]*)", l).group(1))

        # join multiple lines data
        rawdata = filtered
        filtered = []
        merge = False
        line = ""
        for l in rawdata:
            if merge:
                match = re.match(r"[^\'\"]*[\'\"][^\'\"]*$", l)
                line += ' ' + l
                if match is None:
                    merge = True
                else:
                    merge = False
                    filtered.append(line)
            else:
                match = re.match(r"[^\'\"]*[\'\"][^\'\"]*$", l)
                line = l
                if match is not None:
                    merge = True
                else:
                    filtered.append(line)
                    merge = False
        filtered = filtered[1:]

        # do the actual parsing
        rawdata = filtered
        del filtered
        for l in rawdata:
            dataline = re.match(r"(\S+)::(\S+)\s+=\s+(.+)$", l)
            if dataline is not None:
                self.options.append((dataline.group(1), dataline.group(2),\
                    sorted(dataline.group(3).replace('"',\
                        '').replace("'", "").split())))

        for l in rawdata:
            dataline = re.match(r"activethorns\s+=\s+(.+)$", l)
            if dataline is not None:
                self.thorns += dataline.group(1).replace('"', '').split()
            self.thorns = sorted(self.thorns)

    def __repr__(self):
        s = 'activethorns\t\t= "\n'
        for t in self.thorns:
            s += t + '\n'
        s += '"\n\n'

        for o in self.options:
            s += o[0] + '::' + o[1] + '\t\t' + '= '
            try:
                s += str(float(o[2][0]))
            except:
                s += '"'
                if(len(o[2]) > 1):
                    s += '\n'
                    for p in o[2]:
                        s += str(p) + '\n'
                    s += '"\n'
                else:
                    s += str(o[2][0]) + '"'
            s += '\n'
        return s


    def get_options(self, thorn=r'.+', name=r'.+', value=r'.+'):
        """
        Returns a list of options matching the given regular expressions

        * thorn : name of the thorn
        * name  : name of the particular option
        * value : value of the option
        """
        out = []

        for o in self.options:
            s = ''
            for p in o[2]:
                s += str(p) + ' '
            s = s[:-1]

            if re.match(thorn, o[0]) is not None \
                    and re.match(name + '$', o[1]) is not None \
                    and re.match(value + '$', s) is not None:
                out.append(o)
        return out

def parfile_diffs(plist):
    """
    Creates a diff of a list of scidata.cactus.parfile objects

    This returns a list of tuples:
        [(thorn, name, value1, value2, ...)]
    where thorn::name is an option and value1, value2, ... are the different
    values set for that option in the different parfiles
    """
    out = []

    opts = []
    for p in plist:
        for o in p.options:
            opts.append((o[0], o[1]))
    opts = sorted(list(set(opts)))

    for o in opts:
        try:
            oa = plist[0].get_options(thorn=re.escape(o[0]),\
                    name=re.escape(o[1]))[0]
        except:
            oa = (o[0], o[1], '-')
        different = False
        for p in plist[1:]:
            try:
                ob = p.get_options(thorn=re.escape(oa[0]),\
                        name=re.escape(oa[1]))[0]
            except:
                ob = (oa[0], oa[1], '-')
            if oa[2] != ob[2]:
                different = True
        if different:
            L = [oa[0], oa[1], oa[2]]
            for p in plist[1:]:
                try:
                    L.append(p.get_options(thorn=re.escape(oa[0]),\
                            name=re.escape(oa[1]))[0][2])
                except:
                    L.append('-')
            out.append(tuple(L))
    return out

class timerreport:
    """
    Parses/stores the contents of the Cactus TimerReport thorn

    * iteration : dictionary {timer: iteration}
    * time      : dictionary {timer: simulation time}
    * average   : dictionary {timer: average time on all the processors}
    * minimum   : dictionary {timer: minimum time between all the processors}
    * maximum   : dictionary {timer: maximum time between all the processors}
    * name      : dictionary {timer: timer name}
    """

    def __init__(self, filename):
        """
        Initialize the timerreport
        """
        self.iteration = {}
        self.time      = {}
        self.average   = {}
        self.minimum   = {}
        self.maximum   = {}
        self.name      = {}

        rawdata = open(filename, "r").readlines()
        for line in rawdata:
            if line[0] != '#' and len(line) > 1:
                dline = line[:-1].split()

                iteration = int(dline[0])
                time      = float(dline[1])
                index     = int(dline[2])
                average   = float(dline[3])
                minimum   = float(dline[4])
                maximum   = float(dline[5])

                key = str(iteration) + '/' + str(index)

                name = ''
                for p in dline[6:]:
                    name += p + ' '
                name = name[:-1]

                self.iteration[key] = iteration
                self.time[key]      = time
                self.average[key]   = average
                self.minimum[key]   = minimum
                self.maximum[key]   = maximum
                self.name[key]      = name

    def write(self, filename):
        """
        Export the timerreport in CSV format
        """
        csv_writer = csv.writer(open(filename, 'w'), dialect='excel')
        csv_writer.writerow(['Name', 'Average Time', 'Minimum Time',
            'Maximum Time', 'Iteration', 'Simulation time'])
        for key in self.iteration.keys():
            L = []
            L.append(self.name[key])
            L.append(self.average[key])
            L.append(self.minimum[key])
            L.append(self.maximum[key])
            L.append(self.iteration[key])
            L.append(self.time[key])
            csv_writer.writerow(L)
