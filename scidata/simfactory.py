import hashlib
import os
import re
import scidata.cactus
import scidata.utils

class sim:
    """
    A simulation run by simfactory.

    This class contains only very basic informations and is not meant to
    replace simfactory.

    Members:
    * checksum : md5 checksum of the exe file
    * parfile  : a scidata.cactus.parfile object containing the parfile
    * path     : the path of the simulation
    """

    def __init__(self, path):
        """
        Initialize the simulation.

        * path : must be the root path of a simfactory simulation
        """
        self.path = os.path.abspath(path)
        self.parfile = scidata.cactus.parfile(scidata.utils.locate("*.par",\
                os.path.join(self.path, "SIMFACTORY", "par"))[0])
        self.checksum = hashlib.md5(open(scidata.utils.locate("*",\
                os.path.join(self.path, "SIMFACTORY", "exe"))[0]).read()
                ).hexdigest()

class database:
    """
    A database of simulations run by simfactory.

    This is simply a list of simulations found in a given folder.
    """

    def __init__(self, simlist=[]):
        """
        Initialize the database from a list of paths of simulations
        """
        self.simulations = []
        for d in simlist:
            try:
                self.simulations.append(sim(d))
            except:
                pass

    def get_options(self, thorn=r'.+', name=r'.+', value=r'.+', relpath=True):
        """
        Returns a list of option matching the given regular expressions

        Returns a list of tuples [(thorn, name)]

        * thorn   : name of the thorn
        * name    : name of the particular option
        * value   : value of the option
        * relpath : if True we refer to the simulations by their relative paths
        """
        odb = []
        for s in self.simulations:
            lo = s.parfile.get_options(thorn=thorn, name=name, value=value)
            for o in lo:
                if (o[0], o[1]) not in odb:
                    odb.append((o[0], o[1]))
        return odb

    def import_all(self, root=os.path.curdir):
        """
        Fill the database with all the simulations in the given folder
        """
        self.simulations = []
        for d in os.listdir(root):
            if os.path.isdir(os.path.join(d, "SIMFACTORY")):
                self.simulations.append(sim(d))

    def print_diffs(self, relpath=True):
        """
        Returns a table with the difference between the simulations

        * relpath : if True we refer to the simulations by their relative paths
        """
        diffs = scidata.cactus.parfile_diffs(\
                [s.parfile for s in self.simulations])

        table = []
        for d in diffs:
            L = []
            if d[1] is None:
                L.append(d[0])
            else:
                L.append(d[0] + '::' + d[1])

            for q in d[2:]:
                s = ''
                try:
                    s += str(float(q[0]))
                except:
                    if(len(q) > 1):
                        for p in q:
                            s += str(p) + '\n'
                    else:
                        s += str(q[0])
                L.append(s)
            table.append(L)

        return table

    def print_options(self, optlist, relpath=True):
        """
        Returns a table of the given options

        * optlist : must be a list of tuples [(thorn, name)]
        * relpath : if True we refer to the simulations by their relative paths
        """
        table = []
        for o in optlist:
            if o[1] is None:
                L = [o[0]]
            else:
                L = [o[0] + "::" + o[1]]

            for sim in self.simulations:
                try:
                    q = sim.parfile.get_options(thorn=re.escape(o[0]),\
                            name=re.escape(o[1]))[0][2]
                except:
                    q = ['-']
                s = ''
                try:
                    s += str(float(q[0]))
                except:
                    if(len(q) > 1):
                        for p in q:
                            s += str(p) + '\n'
                    else:
                        s += str(q[0])
                L.append(s)
            table.append(L)

        return table

    def print_sims(self, relpath=True):
        """
        Returns a table with the various simulations

        This is suitable to be used as an header for the other print_* methods

        * relpath : if True we refer to the simulations by their relative paths
        """
        dlist = []
        for s in self.simulations:
            if relpath:
                dlist.append(os.path.relpath(s.path))
            else:
                dlist.append(s.path)

        return [[''] + dlist]

    def print_thornlist(self, relpath=True):
        """
        Returns a table with the given thornlist

        * relpath : if True we refer to the simulations by their relative paths
        """
        table = ['activethorns']
        for sim in self.simulations:
            s = ''
            for t in sim.parfile.thorns:
                s += str(t) + '\n'
            table.append(s[:-1])
        return [table]
