import numpy

import scidata.carpet.grid
import scipy.interpolate

class DeltaNDInterpolator(object):
    """
    Delta-function interpolator

    * points : numpy array of points of shape (npoints, ndim)
    * values : numpy array of values of shape (npoints)
    * eps    : delta-function spread: a numpy array of shape (ndim)
    """
    def __init__(self, points, values, eps):
        self.points = points
        self.values = values
        self.eps    = eps

        dummy = numpy.array([float(i) for i in range(len(self.values))])
        self._interp = scipy.interpolate.NearestNDInterpolator(points, dummy)

    def __call__(self, xi):
        """
        Evaluates the interpolated function in the given points

        * xi : numpy array of points of shape (..., ndim)
        """
        index = self._interp(xi)
        try:
            index = list(index)
        except:
            index = [index]
        index = [int(idx) for idx in index]

        mask   = [i for i in range(len(index)) if any(numpy.abs(
            self.points[index[i]] - xi[i]) > self.eps)]
        values = numpy.array([self.values[idx] for idx in index])
        values[mask] = numpy.NaN
        return values

class Interpolator(object):
    """
    Class representing a grid function defined on a Carpet grid hierarchy

    * epsilon : two points are considered to be the same if their distance
                is less than epsilon*[grid spacing] in every direction
    * grid    : a scidata.carpet.grid.grid object
    * interp  : interpolation type
                None: no interpolation. The gridarray can only be evaluated
                      at grid points.
                0   : nearest neighbour interpolation
                1   : linear interpolation
    * mesh    : tuple of grid points
    * rawdata : list of numpy arrays (one for each refinement level)
    """
    def __init__(self, grid, rawdata, interp=None, epsilon=1e-2):
        """
        Initialize the gridarray

        * grid    : must be a scidata.carpet.grid object
        * rawdata : must be a list of numpy arrays (one for each refinement
                    level)
        * interp  : interpolation type
        * epsilon : two points are considered to be the same if their distance
                    is less than epsilon*[grid spacing] in every direction
        """
        assert interp in [None, 0, 1]

        self.grid    = grid
        self.rawdata = rawdata
        self.interp  = []
        self.epsilon = epsilon
        self.coords  = self.grid.coordinates()

        for i in range(len(self.grid)):
            if interp is None:
                mesh = list(numpy.meshgrid(self.coords[i], indexing='ij'))
                points = numpy.column_stack([x.flatten() for x in mesh])
                eps = self.epsilon*self.grid.levels[i].delta
                self.interp.append(DeltaNDInterpolator(points,
                    self.rawdata[i].flatten(), eps))
            else:
                if interp == 0:
                    method = 'nearest'
                else:
                    method = 'linear'
                self.interp.append(scipy.interpolate.RegularGridInterpolator(
                    self.coords[i], self.rawdata[i], method=method,
                    bounds_error=False, fill_value=numpy.nan))

    def __call__(self, xi):
        """
        Evaluates the gridarray in all the given points

        * xi : numpy array of points of shape (..., ndim)
        """
        out    = numpy.empty(xi.shape[0])
        out[:] = numpy.NaN
        for i in reversed(list(range(len(self.grid)))):
            umask      = numpy.isnan(out)
            out[umask] = self.interp[i](xi[umask])
        return out
