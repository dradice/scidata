#!/usr/bin/env python
# This is partly taken from the SciPy Cookbook

import matplotlib.pyplot as plt
import numpy as np
import scidata.kernels as kernels
import scipy.signal as signal

X, Y = np.mgrid[-70:70, -70:70]
Z1  = np.cos((X**2+Y**2)/200.) + np.random.normal(size=X.shape)
Z2 = signal.fftconvolve(Z1, kernels.gaussian(2, 3), mode='same')

plt.figure()
plt.imshow(Z1)

plt.figure()
plt.imshow(Z2)

plt.show()
