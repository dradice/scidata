#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import scidata.carpet.hdf5 as h5
from scidata.carpet.interp import Interpolator

dset = h5.dataset("alp.xz.h5")
grid = dset.get_grid(iteration=0)
data = dset.get_grid_data(grid, iteration=0)

alp = Interpolator(grid, data, None)

x, y = np.mgrid[0:160, 0:160]*0.5
interpdata = alp((x, y))

plt.imshow(interpdata.transpose(), origin='lower', extent=(x.min(), x.max(),
    y.min(), y.max()), interpolation=None)
plt.colorbar()
plt.show()
