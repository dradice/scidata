#!/usr/bin/env python
#
# Computes the norm of a given field (stored in CarpetHDF5 format)
# NOTE: this script assumes a fixed grid

from __future__ import print_function

import numpy as np
import scidata.carpet.hdf5 as h5
import optparse
import time
import sys

from math import sqrt
from scidata.utils import locate

usage = "%prog [options] field"
parser = optparse.OptionParser(usage)
parser.add_option("-n", "--norm", dest="norm", help=
        "Which norm to computed (supported norms: 1, 2, inf) (Defaults to 1)")
parser.add_option("-p", "--plane", dest="plane", help=
        "If given the norm is only computed on the selected plane")
parser.add_option("-r", "--reflevel", dest="reflevel", help=
        "On which refinement level the norm is computed (defaults to 0)")

(options, args) = parser.parse_args()

if len(args) != 1:
    parser.error("No field specified")
field = args[0]

if options.norm is None:
    options.norm = "1"

if options.reflevel is None:
    options.reflevel = "0"
options.reflevel = int(options.reflevel)

sys.stderr.write("Looking for the files to open...")
t0 = time.time()
if options.plane is None:
    flist = locate(field + ".file*.h5")
else:
    flist = locate("%s.%s.h5" % (field, options.plane))
t1 = time.time()
sys.stderr.write("done! ({} sec)\n".format(t1 - t0))

sys.stderr.write("Reading the metadata...")
t0 = time.time()
dset = h5.dataset(flist)
t1 = time.time()
sys.stderr.write("done! ({} sec)\n".format(t1 - t0))

sys.stderr.write("Reconstructing the grid...")
t0 = time.time()
grid = dset.get_reflevel(iteration=dset.iterations[0], reflevel=options.reflevel)
t1 = time.time()
sys.stderr.write("done! ({} sec)\n".format(t1 - t0))

for it in dset.iterations:
    sys.stderr.write("Processing iteration {}...".format(it))
    t0 = time.time()
    var  = np.abs(dset.get_reflevel_data(grid, iteration=it, timelevel=0))
    t1 = time.time()
    sys.stderr.write("done! ({} sec)\n".format(t1 - t0))

    if options.norm == "1":
        print(("%d %.16g %.16g" % (it, grid.time, var.mean())))
    elif options.norm == "2":
        print(("%d %.16g %.16g" % (it, grid.time, sqrt((var**2).mean()))))
    elif options.norm == "inf":
        print(("%d %.16g %.16g" % (it, grid.time, var.max())))
