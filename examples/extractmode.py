#!/usr/bin/env python
# extractmode.py [input_file] [output_file] [frequency]

import numpy as np
import scidata.xgraph as xgraph
import sys

ifile = sys.argv[1]
ofile = sys.argv[2]
freq  = float(sys.argv[3])

data    = xgraph.parsefile(ifile)
x, f, psd = data.spectrum()

psd = 20*np.log10(psd)

fi = f.searchsorted(freq)
fp = f[fi]
fm = f[fi-1]
s  = (freq - fm) / (fp - fm)

eig = []
for i in range(len(x)):
    eig.append(s*psd[i, fi] + (1-s)*psd[i, fi-1])

eig = np.array(eig)

file = open(ofile, 'w')
for i in range(len(x)):
    file.write(str(x[i]) + " " + str(eig[i]) + "\n")
