#!/usr/bin/env python
#
# Plots the velocity field and a color-coded scalar field in a plane

# =============================================================================
# Initialization
# =============================================================================
from __future__ import print_function

import matplotlib
matplotlib.use("Agg")

from glob import glob
import matplotlib.pyplot as plt
import numpy as np
from numpy import inf
import scidata.carpet.hdf5 as hdf5
import scidata.kernels as kernels
import scipy.signal as signal
import optparse
import os

from scidata.utils import *

# =============================================================================
# Default configuration
# =============================================================================
# Which scalar field to plot
field = "rho"
# Which plane are we going to plot xy, xz or yz
plane = "xy"
# Scaling of the velocity vector field (None for the default scaling)
velscale = [1.0, 1.0]
# Seed style
seed_type = 'regular'

# =============================================================================
# Parse CLI
# =============================================================================
usage = "%prog [options]"
parser = optparse.OptionParser(usage)
parser.add_option("-a", "--average", dest="average",\
        help="Smooth the velocity field over the given scale")
parser.add_option("-c", "--contour", dest="contour", action="store_true",\
        help="Do a contour plot of the field")
parser.add_option("-f", "--field", dest="field", help=\
        "Which scalar field to plot [defaults to " + field + "]")
parser.add_option("-g", "--reflevel", dest="reflevel", help=\
        "Which refinement level to plot")
parser.add_option("-l", "--streamlines", action="store_true",\
        dest="streamlines", help="Use streamlines instead of quiver")
parser.add_option("--log", action="store_true",\
        dest="logscale", help="Use a logscale for the scalar field")
parser.add_option("--seed", dest="seed_type", help=\
        "streamline seeds: x, y, regular, random")
parser.add_option("-p", "--plane", dest="plane", help=\
        "Which plane to output [defaults to " + plane + "]")
parser.add_option("-s", "--subtract", action="store_true",\
        dest="subtract", help="Subtract initial value of the scalar field")
parser.add_option("--range", dest="crange", help=\
        "Provide a custom range in the form [vmin, vmax] for plotting")
parser.add_option("-r", "--rescale", dest="rescale", action="store_true",\
        help="Use a different (adapted) scale for each frame")
parser.add_option("-u", "--comoving-x", dest="comoving_x", action="store_true",
        help="Subtract the initial x-velocity field")
parser.add_option("-v", "--comoving-y", dest="comoving_y", action="store_true",
        help="Subtract the initial y-velocity field")
parser.add_option("-x", "--velscale-x", dest="velscalex", help=\
        "Scale the x-velocity by the given factor")
parser.add_option("-y", "--velscale-y", dest="velscaley", help=\
        "Scale the y-velocity by the given factor")
parser.add_option("--xmin", dest="xmin", help="Minimum x")
parser.add_option("--xmax", dest="xmax", help="Maximum x")
parser.add_option("--ymin", dest="ymin", help="Minimum y")
parser.add_option("--ymax", dest="ymax", help="Maximum y")

(options, args) = parser.parse_args()

if options.comoving_x is None:
    options.comoving_x = False

if options.comoving_y is None:
    options.comoving_y = False

if options.crange is not None:
    options.crange = eval(options.crange)

if options.field is not None:
    field = options.field

if options.reflevel is None:
    options.reflevel = 0
else:
    options.reflevel = int(options.reflevel)

if options.plane is not None:
    plane = options.plane

if options.logscale is None:
    options.logscale = False

if options.rescale is None:
    options.rescale = False

if options.seed_type is not None:
    seed_type = options.seed_type

if options.streamlines is None:
    options.streamlines = False

if options.subtract is None:
    options.subtract = False

if options.velscalex is not None:
    velscale[0] = float(options.velscalex)

if options.velscaley is not None:
    velscale[1] = float(options.velscaley)

if options.xmin is None:
    options.xmin = -inf
else:
    options.xmin = float(options.xmin)

if options.xmax is None:
    options.xmax = inf
else:
    options.xmax = float(options.xmax)

if options.ymin is None:
    options.ymin = -inf
else:
    options.ymin = float(options.ymin)

if options.ymax is None:
    options.ymax = inf
else:
    options.ymax = float(options.ymax)

# =============================================================================
# Actual configuration
# =============================================================================
# Output format
format = "png"
# Number of levels for the colormap
nlevels = 20
# Output directory
outdir = "vel." + field + ".r" + str(options.reflevel) + "." + plane +\
        ".output"
# Output filename prefix (the output will be prefix.iteration.format)
prefix = "vel." + field + ".r" + str(options.reflevel) + "." + plane

# =============================================================================
# Main
# =============================================================================
if not os.path.isdir(outdir):
    os.mkdir(outdir)

filenames = {
    "xy" : (field + ".xy.h5", "vel?0?.xy.h5", "vel?1?.xy.h5"),
    "xz" : (field + ".xz.h5", "vel?0?.xz.h5", "vel?2?.xz.h5"),
    "yz" : (field + ".yz.h5", "vel?1?.yz.h5", "vel?2?.yz.h5")
}[plane]

datafiles = [hdf5.dataset(locate(filenames[0])),\
        hdf5.dataset(locate(filenames[1])),\
        hdf5.dataset(locate(filenames[2]))]

# Get grid structure (here we assume that we don't do any regridding, but
# we allow the domain decomposition to change with the iterations)
X, Y = datafiles[0].get_reflevel(reflevel=options.reflevel).mesh()

if options.average is None:
    Xv, Yv = X, Y
else:
    Xv = maskdata(X, 2*int(options.average)*np.ones(2))
    Yv = maskdata(Y, 2*int(options.average)*np.ones(2))

# Store initial field density
grid = datafiles[0].get_reflevel(reflevel=options.reflevel)
field_init = datafiles[0].get_reflevel_data(grid)
v1_init = datafiles[1].get_reflevel_data(grid)
v2_init = datafiles[2].get_reflevel_data(grid)

# Compute contour levels
if not options.rescale and options.crange is None:
    print(("Generating global field density levels..."), end=' ')
    field_min = []
    field_max = []
    for iter in datafiles[0].iterations:
        grid = datafiles[0].get_reflevel(iteration=iter,
                reflevel=options.reflevel)
        field = datafiles[0].get_reflevel_data(grid, iteration=iter)
        if options.subtract:
            field = field - field_init
        if options.logscale:
            field = np.log10(field)
#       field = field / field_init
        field_min.append(field.min())
        field_max.append(field.max())
    field_min = min(field_min)
    field_max = max(field_max)
    step    = (field_max - field_min)/nlevels
    levels  = np.arange(field_min, field_max, step)
    print("done!")

if options.crange is not None:
    field_min = options.crange[0]
    field_max = options.crange[1]
    step = (field_max - field_min) / nlevels
    levels = np.arange(field_min, field_max, step)

# Do the actual plotting
iterfill = len(str(datafiles[0].iterations[-1]))
framefill = len(str(len(datafiles[0].iterations)))
for k in range(len(datafiles[0].iterations)):
    iter = datafiles[0].iterations[k]
    print(("Processing frame " + str(k).zfill(framefill) + "/" +\
            str(len(datafiles[0].iterations)-1) + "..."), end=' ')

    grid = datafiles[0].get_reflevel(iteration=iter,
            reflevel=options.reflevel)

    X, Y   = grid.mesh()
    if options.average is None:
        Xv, Yv = X, Y
    else:
        Xv = maskdata(X, 2*int(options.average)*np.ones(2))
        Yv = maskdata(Y, 2*int(options.average)*np.ones(2))

    field  = datafiles[0].get_reflevel_data(grid, iteration=iter)
    v1   = datafiles[1].get_reflevel_data(grid, iteration=iter)
    v2   = datafiles[2].get_reflevel_data(grid, iteration=iter)

    if options.comoving_x:
        v1 = v1 - v1_init
    if options.comoving_y:
        v2 = v2 - v2_init

    v1   = v1 * velscale[0]
    v2   = v2 * velscale[1]

    if options.subtract:
        field = field - field_init
    if options.logscale:
        field = np.log10(field)
#    field = field / field_init

    if options.rescale:
        field_min = field.min()
        field_max = field.max()
        step = (field_max - field_min) / nlevels
        levels = np.arange(field_min, field_max, step)

    if options.contour:
        plot = plt.contourf(X, Y, field, levels)
    else:
        dw = grid.origin
        up = grid.upperbound()
        plot = plt.imshow(field.transpose(), origin='lower', vmin=field_min,\
            vmax=field_max, extent=(dw[0], up[0], dw[1], up[1]), aspect='auto')
    cbar = plt.colorbar(plot)

    if options.average is not None and not options.streamlines:
        v1 = signal.convolve(v1, kernels.gaussian(2,\
                int(options.average)), mode='same')
        v1 = maskdata(v1, 2*int(options.average)*np.ones(2))

        v2 = signal.convolve(v2, kernels.gaussian(2,\
                int(options.average)), mode='same')
        v2 = maskdata(v2, 2*int(options.average)*np.ones(2))

    if options.streamlines:
        arpl = streamlines(X, Y, v1, v2, lfac=10, seed_type=seed_type, n=20)
    else:
        arpl = plt.quiver(Xv, Yv, v1, v2, angles='xy', scale_units='xy',\
                scale=1.0, units='xy')

    dset = datafiles[0].get_dataset(iteration=iter)
    tstr = 'time = %10.2f' % dset.attrs['time']
    plt.title("vec. scale = " + str(velscale) + "    " + tstr)

    bound = list(plt.axis())
    bound[0] = max(bound[0], options.xmin)
    bound[1] = min(bound[1], options.xmax)
    bound[2] = max(bound[2], options.ymin)
    bound[3] = min(bound[3], options.ymax)
    plt.axis(bound)

    plt.savefig(outdir + "/" + prefix + "." +\
            str(iter).zfill(iterfill) + "." + format)

    plt.close()

    print("done!")
