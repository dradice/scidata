#!/usr/bin/env python

from distutils.core import setup
from glob import glob

setup(
        name = 'scidata',
        version = '0.1',
        description = 'Scientific data library',
        author = 'David Radice',
        author_email = 'david.radice@aei.mpg.de',
        url = 'https://bitbucket.org/dradice/scidata',
        packages = ['scidata', 'scidata/carpet','scidata/athena'],
        requires = ['h5py', 'numpy', 'matplotlib', 'texttable'],
        scripts = glob("./examples/*.py")
        )
